import {describe, expect, test} from '@jest/globals';
import lexer from '../lexer';
import { evaluateRPN } from './eval';
import { parse } from './parser';
import { toString } from './queue';

describe('parser', () => {
    test('parse', () => {
        const tokens = [...lexer('(10 + 12) + cos(1-1) + 2 - (2 + 5)*7 / (7)')].filter((token) => token.type !== 'whitespace' && token.type !== 'EOF');
        const result = parse(tokens);
        const str = toString(result)
        const evaled =  evaluateRPN(result);
        expect(evaled).toBe(18);
    });

    test('cos', () => {
        const tokens = [...lexer('cos(1-1) - 1')].filter((token) => token.type !== 'whitespace' && token.type !== 'EOF');
        const result = parse(tokens);
        const str = toString(result)
        const evaled =  evaluateRPN(result);
        expect(evaled).toBe(0);
    })
}

);