
// stack structure

export interface Stack<T> {
    push: (item: T) => void;
    pop: () => T | undefined;
    peek: () => T | undefined;
    isEmpty: () => boolean;
    size: () => number;
    clear: () => void;
    items: T[]

}

export function stackFactory<T>(): Stack<T> {
    const items: T[] = [];
    return {
        items: items,
        push: (item: T) => items.push(item),
        pop: () => items.pop(),
        peek: () => items[items.length - 1],
        isEmpty: () => items.length === 0,
        size: () => items.length,
        clear: () => items.splice(0, items.length),
    };
}
