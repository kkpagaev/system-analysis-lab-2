import { Token } from "../lexer/lexer";
import { Queue, queueFactory } from "./queue";
import { stackFactory } from "./stack";
// converts infix to 

export function parse(tokens: Token[]): Queue<Token> {
    // for operators and brackets
    const stack = stackFactory<Token>();    // for numbers
    const queue = queueFactory<Token>();
    for (const token of tokens) {
        if (token.type === "number") {
            queue.enqueue(token);
            continue
        } 
        if (stack.isEmpty()) {
            stack.push(token);
            continue
        }
        if (token.type === 'function') {
            stack.push(token);
            continue
        }
        if (token.value === "(") {
            stack.push(token);
            continue
        }
        if (token.value === ")") {
            while (stack.peek().value !== "(") {
                queue.enqueue(stack.pop());
            }
            stack.pop();
            continue
        }
        if (token.value === "+" || token.value === "-") {
            while (!stack.isEmpty() && stack.peek().value !== "(") {
                queue.enqueue(stack.pop());
            }
            stack.push(token);
            continue
        }
        if (token.value === "*" || token.value === "/") {
            while (!stack.isEmpty() && (stack.peek().value === "*" || stack.peek().value === "/")) {
                queue.enqueue(stack.pop());
            }
            stack.push(token);
            continue
        }

    }
    while (!stack.isEmpty()) {
        queue.enqueue(stack.pop());
    }
    return queue;
}

