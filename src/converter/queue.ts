
//queue 

import { Token } from "../lexer/lexer";

export interface Queue<T> {
    enqueue: (item: T) => void;
    dequeue: () => T | undefined;
    peek: () => T | undefined;
    isEmpty: () => boolean;
    size: () => number;
    clear: () => void;
    items: T[]
}

export function queueFactory<T>(): Queue<T> {
    const items: T[] = [];
    return {
        items: items,
        enqueue: (item: T) => items.push(item),
        dequeue: () => items.shift(),
        peek: () => items[0],
        isEmpty: () => items.length === 0,
        size: () => items.length,
        clear: () => items.splice(0, items.length),
    };
}

export function toString(queue: Queue<Token>) {
    return queue.items.join(' ')
}
