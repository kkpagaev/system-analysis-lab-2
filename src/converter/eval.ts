import { Token } from "../lexer/lexer";
import { Queue } from "./queue";
import { stackFactory } from "./stack";

export function evaluateRPN(queue: Queue<Token>) {
    const stack = stackFactory<number>();    // for numbers
    while(!queue.isEmpty()) {
        const token = queue.dequeue()
        if(token.type === 'number') {
            stack.push(+token.value)
            continue
        }
        if(token.type === 'operator') {
            const right = stack.pop()
            const left = stack.pop()
            stack.push(operate(token.value, +right, +left))
            continue
        }
        if(token.type === 'function') {
            const val = stack.pop()
            stack.push(evalFunc(token.value, val))
            continue
        }
        throw new Error("invalid token type: " + token.type)
    }
    return stack.pop()
}

function operate(operation: string, right: number, left:number): number {
    switch(operation) {
        case '+':
            return left + right;
        case '-':
            return left - right;
        case '*':
            return left * right;
        case '/':
            return left / right;
        default: 
            throw new Error("invalid operation: " + operation)
    }
}

function evalFunc(func: string, val: number): number {
    switch(func) {
        case 'sin':
            return Math.sin(val);
        case 'cos':
            return Math.cos(val);
        default: 
            throw new Error("invalid function: " + func)
    }
}
