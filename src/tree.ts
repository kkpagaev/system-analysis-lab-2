class L {
    constructor(public length: number, public value: number, public b: B, public l?: L) { }
}

class B {
    constructor(public value: number) { }
}

class S {
    public value: number;
    constructor(public left: L, public right: L) {
        this.value = left.value + right.value
    }
}

const PHI = (1 + Math.sqrt(5)) / 2

function getVal(power, char) {
    switch (char) {
        case '0':
            return 0;
        case '1':
            return PHI ** power;
        default:
            throw new Error('unknown char: ' + char)
    }
}

function build(str: string, f: (i: number) => number) {
    const [x, ...xs] = str
    const initialVal = getVal(f(1), x)
    const initial = new L(1, initialVal, new B(parseInt(x)))

    return xs.reduce((node, char, i) => {
        const length = i + 2
        const val = getVal(f(length), char)
        const sum = node.value + val

        return new L(length, sum, new B(parseInt(char)), node)
    }, initial)
}

export function buildTree(str: string) {
    const [left, right] = str.split('.')
    const leftLeef = build(left, (i) => left.length - i)
    const rightLeef = build(right, (i) => -1 * i)
    return new S(leftLeef, rightLeef)
}
