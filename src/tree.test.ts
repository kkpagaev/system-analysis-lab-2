import { describe, expect, test } from '@jest/globals';
import { buildTree } from './tree';

describe('Lexer', () => {
    test('should be 3', () => {
        const str = '100.01';
        const tr = buildTree(str);
        const val = tr.value;
        expect(val).toBe(3);
    });
    test('should be 4', () => {
        const str = '101.01';
        const tr = buildTree(str);
        const val = tr.value;
        expect(val).toBe(4);
    });
    test('should be 5', () => {
        const str = '1000.1001';
        const tr = buildTree(str);
        const val = tr.value;
        expect(val).toBe(5);
    });

    test('should be 6', () => {
        const str = '1010.0001';
        const tr = buildTree(str);
        const val = tr.value;
        expect(val).toBe(6);
    } );

    test('should be 7', () => {
        const str = '10000.0001';
        const tr = buildTree(str);
        const val = tr.value;
        expect(val).toBe(7);
    });

    test('should be 8', () => {
        const str = '10001.0001';
        const tr = buildTree(str);
        const val = tr.value;
        expect(val).toBe(8);
    }
    );

    test('should be 9', () => {
        const str = '10010.0001';
        const tr = buildTree(str);
        const val = tr.value;
        expect(val).toBe(9);
    }
    );

    test('should be 10', () => {
        const str = '10011.0001';
        const tr = buildTree(str);
        const val = tr.value;
        expect(val).toBe(10);
    }
    );

});
