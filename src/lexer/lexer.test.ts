import {describe, expect, test} from '@jest/globals';
import {lexer} from './lexer';

describe('Lexer', () => {
  test('numbers and opertator should pass', () => {
    const code = '31 -23 + 123';
    expect(() => [...lexer(code)]).not.toThrow();
  });

  test('numbers and opertator should pass', () => {
    const code = '3 / 123  ( 12 ) * 3';
    expect(() => [...lexer(code)]).not.toThrow();
  });

  test('should be all spaces', () => {
    const code = '   ';
    const tokens = [...lexer(code)];
    expect(tokens[0].type).toBe('whitespace');
    expect(tokens[1].type).toBe('whitespace');
    expect(tokens[2].type).toBe('whitespace');
  });

  test('should be all spaces', () => {
    const code = 'sin(2 + 2)';
    expect(() => [...lexer(code)]).not.toThrow();
  });
});
