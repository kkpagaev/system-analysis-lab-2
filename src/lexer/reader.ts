export interface Reader {
  next: () => string | undefined;
  peek: () => string | undefined;
  position: () => number;
}

export function readerFactory(input: string) {
  let position = 0;
  return {
    next: () => {
      if (position < input.length) {
        return input[position++];
      }
      return undefined;
    },
    peek: () => {
      if (position < input.length) {
        return input[position];
      }
      return undefined;
    },
    position: () => position,
  };
}
