import {Token} from '../lexer';
import {Reader} from '../reader';

export function comment(reader: Reader): Token | null {
  const char = reader.peek();
  if (!(char && char === '#')) {
    return null;
  }
  reader.next();
  return {
    type: 'comment',
    position: reader.position() - 1,
    length: 1,
    value: char,
  };
}
