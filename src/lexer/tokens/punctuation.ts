import {Token} from '../lexer';
import {Reader} from '../reader';

const punctuationChars = ['(', ')'];

export function punctuation(reader: Reader): Token | null {
  const char = reader.peek();
  if (!(char && punctuationChars.includes(char))) {
    return null;
  }
  reader.next();
  return {
    type: 'punctuation',
    position: reader.position() - 1,
    length: 1,
    value: char,
  };
}
