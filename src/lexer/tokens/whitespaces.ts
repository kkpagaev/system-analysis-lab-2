import {Token} from '../lexer';
import {Reader} from '../reader';

const whitespaceChars = [' ', '\t', '\n', '\r'];

export function whitespace(reader: Reader, value = ''): Token | null {
  const char = reader.peek();
  if (!(char && whitespaceChars.includes(char))) {
    return null;
  }
  reader.next();
  return {
    type: 'whitespace',
    position: reader.position() - value.length,
    length: value.length,
    value: value,
  };
}
