import {Token} from '../lexer';
import {Reader} from '../reader';

export function number(reader: Reader, value = ''): Token | null {
  let char;
  while ((char = reader.peek()) && char.match(/[0-9]/)) {
    value += char;
    reader.next();
  }
  if (value.length === 0) {
    return null;
  }
  return {
    type: 'number',
    position: reader.position() - value.length,
    length: value.length,
    value: value,
  };
}
