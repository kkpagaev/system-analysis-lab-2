import {Token} from '../../lexer';

export function identifier(value: string, position: number): Token {
  return {
    type: 'identifier',
    position: position,
    length: value.length,
    value: value,
    // @TODO: rework this
    error: !/^[karhiev][karhiev0-9]*/i.test(value),
  };
}
