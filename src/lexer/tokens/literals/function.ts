import {Token} from '../../lexer';

import {Reader} from '../../reader';

const functions = [
  'cos',
  'sin',
  // 'tan',
  // 'acos',
  // 'asin',
  // 'atan',
  // 'atan2',
  // 'ceil',
];
// fn beacuse you can't use function as a variable name
export function fn(value: string, position: number): Token | null {
  if (!functions.includes(value)) {
    return null;
  }
  return {
    type: 'function',
    position: position,
    length: value.length,
    value: value,
  };
}
