import {Token} from '../lexer';
import {Reader} from '../reader';

const operatorChars = [
  '+',
  '-',
  '*',
  '/',
];

export function operator(reader: Reader): Token | null {
  const char = reader.peek();
  if (!(char && operatorChars.includes(char))) {
    return null;
  }
  reader.next();
  return {
    type: 'operator',
    position: reader.position() - 1,
    length: 1,
    value: char,
  };
}
