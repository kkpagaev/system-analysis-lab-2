import {EOF} from './tokens/EOF';
import {number} from './tokens/number';
import {readerFactory} from './reader';
import {whitespace} from './tokens/whitespaces';
import {operator} from './tokens/operators';
import {comment} from './tokens/comment';
import {litiral} from './tokens/literals/litiral';
import {punctuation} from './tokens/punctuation';

type TokenType =
  | 'number'
  | 'identifier'
  | 'keyword'
  | 'function'
  | 'operator'
  | 'whitespace'
  | 'comment'
  | 'EOF'
  | 'punctuation';

export interface Token {
  type: TokenType;
  position: number;
  length: number;
  value?: string;
  error?: boolean;
}

export function* lexer(input: string): Generator<Token> {
  const reader = readerFactory(input);
  let char;
  while ((char = reader.peek())) {
    const token =
      whitespace(reader) ||
      number(reader) ||
      punctuation(reader) ||
      operator(reader) ||
      litiral(reader);
    if (token === null) {
      throw new Error(`Invalid character: ${char}`);
    }
    yield token;
  }

  yield EOF(reader);
}
